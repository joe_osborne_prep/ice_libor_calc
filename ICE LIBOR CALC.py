from xlrd import open_workbook
import numpy as np
import warnings

'''

'''
def extractICE (workbook, ibaCurrency):
    currency = {}
    for sheet in workbook.sheets():
        nRows = sheet.nrows
        nCols = sheet.ncols
        for row in range(0, nRows):
            for col in range(nCols):
                value  = (sheet.cell(row,col).value)
                value2 = (sheet.cell(row,col-1).value)
                if (value2 in (ibaCurrency) ):
                    if value2 in currency:
                        tempL = currency.get(value2)
                        tempL.append(value)
                        tempL.sort(reverse = True)
                        currency.update({value2:tempL})
                    else:
                        vl=[value]
                        currency.update({value2:vl})
    return currency

'''

'''
def calcICELIBOR(ccy,libVal):
    try:
        valueList = libVal.get(ccy)
        trimSize = round(((len(valueList)) - 0.01) / 4)
        valueList = valueList[trimSize:]
        valueList = valueList[:-trimSize]
        iceval = np.mean(valueList)
        return iceval
    except(RuntimeWarning,TypeError):
        print("There was not enough recorded data for this currency.")
    return 0
###############################################################################

'''

'''
acceptedCCY = ['EUR','GBP','USD','CHF','JPY']
open = False

while(open == False):
    try:
        file = input("Please type the name of you ICE LIBOR values file: \n")
        file = file.split(".",1)[0] + ".xlsx"
        wb = open_workbook(file)
        open = True
    except (FileNotFoundError):
        print("Incorrect file name.\nPlease try again.")
        open = False

liborValues = extractICE(wb,acceptedCCY)

print()
print("1=EUR    2=GBP   3=USD   4=CHF   5=JPY   6=EXIT")
print()
running=True
while(running):
    userCCY = int(input("Please enter a number for your chosen currency: \n"))
    if(userCCY == 6):
        print("Goodbye")
        running = False

    else:
        chosenCurrency = acceptedCCY[userCCY-1]
        warnings.filterwarnings("error")
        totIce = calcICELIBOR(chosenCurrency,liborValues)
        print("ICE LIBOR rate for",chosenCurrency,": ",totIce)
        print()